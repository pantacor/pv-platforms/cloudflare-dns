#!/bin/bash

while :
do
	sleep 1
	if [ -f /pantavisor/user-meta/dns-extra.conf ] && ! diff -q /pantavisor/user-meta/dns-extra.conf /etc/dnsmasq.d/dns-extra.conf; then
	        cp /pantavisor/user-meta/dns-extra.conf /etc/dnsmasq.d/dns-extra.conf
		/etc/init.d/dnsmasq stop
		/etc/init.d/dnsmasq start
		continue
	fi
	if [ -f /pantavisor/user-meta/cloudflared.upstream ] && ! diff -q /pantavisor/user-meta/cloudflared.upstream /run/cloudflared.upstream; then
	        cp /pantavisor/user-meta/cloudflared.upstream /run/cloudflared.upstream
		/etc/init.d/cloudflared stop
		/etc/init.d/cloudflared start
		continue
	fi
	rc-service cloudflared status
	if [ "$?" != "0" ]; then
		/etc/init.d/cloudflared stop
		/etc/init.d/cloudflared start
	fi
	if ! dig @127.0.0.1 -p 8253 pantahub.com +timeout=5 +short | grep -q [0-9]; then
		/etc/init.d/cloudflared stop
		/etc/init.d/cloudflared start
	fi
	sleep 5
done
